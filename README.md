# How to use FSL

---

## これは何

FSLのマニュアル．

FSLを使うとfMRI解析やDTI解析ができるよ．

## 目次

+ [インストール](https://bitbucket.org/Kodama_Takeru/how-to-use-fsl/src/master/dist/install.md)
+ [FEAT](./dist/FEAT.md)

---
Author: 小玉　健
