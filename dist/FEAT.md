# FEAT is 何

fMRI解析ができるよ。

## 使い方

1.  FEATを開く。[画面](./img/feat.png)
2.  Select 4D dateをクリック。開いたダイアログにexpデータを指定。[画面](./img/feat1.png)
3.  Regisrationタブをクリック。Main structural imageに構造脳を指定。[画面](./img/feat2.png)
4.  Statsタブをクリック。Model setup wizardをクリック。r(rest)period(s)とA period(s)を20に設定。その後Processをクリック。[画面](./img/feat3.png)
5.  Miscタブをクリック。Estimate from data, Estimate High Pass Filterを順にクリック。そしてGo。[画面](./img/feat4.png)
6.  Utils > High-res ~~~ をクリック。Select a FEAT directoryにFEAT解析結果のディレクトリを入力。その後，GOをクリック。[画面](./img/feat5.jpg)

## 画像の見方

1.   fsleyesを起動。
2.   Overlay List左の'+'をクリック[画像](./img/feat6.jpg)。FEAT解析結果のディレクトリ内の'hr'フォルダ内の"renderd_thresh_zstat1.nii.gz"で脳画像 + 賦活，"thresh_zstat1.nii.gz"で賦活のみが見られる。表示の仕方を[画像](./img/feat7.jpg)を参照して"Render1t"に変更。
3.   メニューバー > Settings > Ortho View 1 > Atlas panelより脳部位を調べられる。[画面](./img/feat8.jpg)
