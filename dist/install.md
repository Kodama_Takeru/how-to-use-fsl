# FSLのインストール
---

## Mac

1. [XQuartz](https://www.xquartz.org)のインストール．
2. FSLをダウンロード. ```$ wget https://fsl.fmrib.ox.ac.uk/fsldownloads/fslinstaller.py```
3. ``` $ python fslinstall.py``` 後，Enterを押し続ければインストールされる．
4. ```$ echo $FSLDIR```で環境変数が整っていることを確認する．